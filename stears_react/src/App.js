import React from 'react';
import { Switch, Route, Link, BrowserRouter as Router } from "react-router-dom";
import Home  from "./components/Home";
import Companies  from "./components/Companies";
import Company  from "./components/Company";

function App() {
  return (
    <Router>
      <div className="App">
        <Link to="/"><h3 className="App-header">Stear Companies</h3></Link>
      </div>
      <div className="container">
        <Switch>
          <Route exact path="/companies" component={Companies}/>
          <Route exact path="/companies/:companyId" component={Company}/>
          <Route component={Home}/>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
