const companies = [
				{
					id: 1,
					name: "Microsoft",
					sector : "Technology",
					email : "admin@microsoft.com",
					phone : "08033999449",
					address : "23, Akin Adeshola Street VI, Lagos.",
					website : "www.microsoft.com",
					reports: [
						{
							company: "Microsoft Ng",
							type: "Budget",
							assignee: "Ajayi Nurudeen",
							deadline: "14/04/2020",
							time: "10:23 PM",
						},
						{
							company: "Microsoft ZA",
							type: "Budget",
							assignee: "Ajayi Nurudeen",
							deadline: "14/04/2020",
							time: "10:23 PM",
						},
						{
							company: "Microsoft UK",
							type: "Booking",
							assignee: "Ajayi Nurudeen",
							deadline: "14/04/2020",
							time: "10:23 PM",
						}
					]
				},
				{
					id: 2,
					name: "Amazon",
					sector : "Technology",
					email : "admin@aws.com",
					phone : "08033999449",
					address : "23, Akin Adeshola Street VI, Lagos.",
					website : "www.aws.com",
					reports: []
				},
				{
					id: 3,
					name: "Apple",
					sector : "Technology",
					email : "admin@apple.com",
					phone : "08033999449",
					address : "23, Akin Adeshola Street VI, Lagos.",
					website : "www.apple.com",
					reports: [
						{
							company: "Apple US",
							type: "Budget",
							assignee: "Francis Bishop",
							deadline: "14/04/2020",
							time: "07:56 PM",
						},
						{
							company: "Apple US",
							type: "Budget",
							assignee: "Francis Bishop",
							deadline: "14/04/2020",
							time: "07:56 PM",
						}
					]
				},
				{
					id: 4,
					name: "Google",
					sector : "Technology",
					email : "admin@google.com",
					phone : "08033999449",
					address : "23, Akin Adeshola Street VI, Lagos.",
					website : "www.google.com",
					reports: []
				},
				{
					id: 5,
					name: "Alibaba",
					sector : "E-commerce",
					email : "admin@alibaba.com",
					phone : "08033999449",
					address : "23, Akin Adeshola Street VI, Lagos.",
					website : "www.alibaba.com",
					reports: []
				}
			];

export default companies;