import React, { Component } from 'react';

export default class ReportEntry extends Component {
	render() {
		const { index, report } = this.props;
		return (
			<tr>
	            <th scope="row">{index}</th>
	            <td>{report.company}</td>
	            <td>{report.type}</td>
	            <td>{report.time}</td>
	            <td>{report.assignee}</td>
	            <td>{report.deadline}</td>
	        </tr>
		);
	}
}
