import React, { Component, Fragment } from 'react';
import CompanyCard from "./CompanyCard";
import companies from "../data";

export default class Companies extends Component {

	constructor(props) {
		super(props);
		this.state = {
			companies: companies
		}
	}

	handleClick = id => {
		this.props.history.push(`/companies/${id}`)
	}

	render() {
		const { companies } = this.state;

		return (
			<Fragment>
				<div className="row justify-content-md-center">
					<div className="col-2"></div>
					<div className="col-md-auto">
							<h4 className="page-title">
								All Companies
							</h4>
					</div>
					<div className="col-2"></div>
				</div>

				<div className="row">
					{
						companies.map((company, index) => (
							<CompanyCard 
								key={index}
								company={company}
								click={() => this.handleClick(company.id)}
							/>
						))
					}
	            </div>
            </Fragment>
		);
	}
}