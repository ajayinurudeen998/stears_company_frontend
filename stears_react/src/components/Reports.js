import React, { Component } from 'react';
import ReportEntry from "./ReportEntry";

export default class Reports extends Component {
	render() {
		const { reports } = this.props;
		return reports && reports.length ? (
			<table class="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">S/N</th>
                    <th scope="col">Company</th>
                    <th scope="col">Report Type</th>
                    <th scope="col">Time</th>
                    <th scope="col">Assignee</th>
                    <th scope="col">Deadline</th>
                  </tr>
                </thead>
                <tbody>
                	{
                		reports.map((report, index) => (
                			<ReportEntry 
                				report={report} 
                				key={index}
                				index={index}
            				/>
            			))
                	}
                </tbody>
           	</table>
		): (
			<h4 className="text-danger">No reports found</h4>
		);
	}
}
