import React, { Component, Fragment } from 'react';
import companies from "../data";
import CompanyCard from "./CompanyCard";
import Reports from "./Reports";
import { Link } from "react-router-dom";

export default class Company extends Component {
	render() {
		const id = parseInt(this.props.match.params.companyId);
		const company = companies.find(company => id === company.id);

		return company ? (
			<Fragment>
				<div className="back-link col">
					<Link to="/companies">
						<img src="/back.svg" alt="back" />
					</Link>
				</div>
				<CompanyCard company={company} />
				<div className="col">
					<h5>Reports</h5>
					<hr/>
					<Reports reports={company.reports} />
				</div>
			</Fragment>
		): (
			<div><br/><h3 className="text-danger text-center">Company Not Found</h3></div>
		);
	}
}
