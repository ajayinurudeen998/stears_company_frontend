import React, { Component } from 'react';

export default class CompanyCard extends Component {
	render() {
		const { company, click } = this.props; 
		return (
			<div className={click ? "col-lg-4 col-md-6": "col"}>
                <div className={`card ${click ? "clickable": ""}`} onClick={click}> 
                    <div className="card-body">
                        <h5 className="card-title">{company.name}</h5>
                        <h6 className="card-subtitle mb-3 text-muted">{company.sector}</h6>
                        <p className="card-text"><strong>Address: </strong>{company.address}</p>                            
                        <p className="card-text"><strong>Tel: </strong>{company.phone}</p>                            
                        <p className="card-text"><strong>Website: </strong>{company.website}</p>                            
                        <p className="card-text"><strong>Email: </strong>{company.email}</p>                            
                    </div>
                </div>
            </div>
		);
	}
}
