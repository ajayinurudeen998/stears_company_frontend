import React, { Component } from 'react';
import { Link } from "react-router-dom";

export default class Home extends Component {
	render() {
		return (
			<div className="home">
				<h1>Welcome</h1>
				<Link to="/companies"><h5>All Companies</h5></Link>
			</div>
		);
	}
}
